FROM python:3.6
MAINTAINER mr.eightnoteight@gmail.com

COPY artemis /artemis
COPY requirements.txt /artemis/requirements.txt

RUN mkdir -p /data
RUN pip install -r /artemis/requirements.txt

VOLUME /data

CMD ["bash", "/artemis/run.sh"]

