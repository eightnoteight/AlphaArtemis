import base64
import sys
import requests
import os

payload = base64.encodebytes(bytes(sys.stdin.read(), encoding='utf-8'))

if os.environ.get('HOST', '') != 'mark':
    secrets_url = 'https://secrets:9001/alpha_artemis.eightnoteight.smartzero'
else:
    secrets_url = 'https://localhost:9001/alpha_artemis.eightnoteight.smartzero'

print(requests.put(secrets_url, data=payload, verify=False).status_code)

