import json
import requests
import re
import os
import time
import telegram
import praw
import imgurpython
import base64
import traceback
import logging
import html
import textwrap

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)


secrets_url = 'https://secrets:9001/alpha_artemis.eightnoteight.smartzero'

secrets = json.loads(str(base64.decodebytes(requests.get(secrets_url, verify=False).content), encoding='utf-8'))

bot = telegram.Bot(token=secrets['telegram_bot_api_token'])
imgur_client = imgurpython.ImgurClient(client_id=secrets['imgur_client_id'],
                                       client_secret=secrets['imgur_client_secret'])
reddit_client = praw.Reddit(client_id=secrets['reddit_cilent_id'],
                            client_secret=secrets['reddit_client_secret'],
                            user_agent='AlphaArtemis')


def main(reddit_channel, telegram_channel, boards):
    # retrieve the posts
    posts = set()
    sub_reddit = reddit_client.subreddit(reddit_channel)
    logging.debug(msg=f'main('
                      f'reddit_channel={reddit_channel}, '
                      f'telegram_channel={telegram_channel}, '
                      f'boards={boards})')

    for board in boards:
        posts.update(
            list({
                     'hot': sub_reddit.hot,
                     'new': sub_reddit.new,
                     'controversial': lambda: sub_reddit.controversial('day'),
                     'top': lambda: sub_reddit.top('day'),
                     'rising': sub_reddit.rising,
                 }[board]()))

    # delete duplicates based on the history
    previous_posts_id_file = f'/data/previous_posts_id.{reddit_channel}'
    if os.path.exists(previous_posts_id_file):
        previous_posts_id = json.load(open(previous_posts_id_file, 'r'))
    else:
        previous_posts_id = []

    previous_posts_id_set = set(previous_posts_id)

    for pid, post in {post.id: post for post in posts}.items():
        if pid in previous_posts_id_set:
            posts.remove(post)
    previous_posts_id = [post.id for post in posts] + previous_posts_id
    del previous_posts_id[2000:]

    # save history based on the new ones and old ones
    json.dump(previous_posts_id, open(previous_posts_id_file, 'w'))

    # send telegram msg
    for post in posts:
        title = post.title
        text_message = post.selftext or ''
        reddit_link = 'https://www.reddit.com' + post.permalink
        logging.debug(f'title={title}\n'
                      f'text_message={text_message}\n'
                      f'reddit_link={reddit_link}')
        photos = []
        if 'image' in requests.head(post.url).headers['content-type']:
            photos = [post.url]
        elif 'imgur.com' in post.domain:
            entrypoint = post.url.replace('http://', '').replace('https://', '')
            entrypoint_regexes = [
                r'^imgur.com/(?P<image>\w+)$',
                r'^imgur.com/(?P<image>\w+)\.(\w+)$',
                r'^i.imgur.com/(?P<image>\w+)\.(\w+)$',
                r'^imgur.com/a/(?P<album>\w+)$',
                r'^imgur.com/a/(\w+)#(?P<image>\w+)$',
                r'^imgur.com/gallery/(?P<gallery>\w+)$',
            ]
            logging.debug(f'entrypoint={entrypoint}')
            for regex in entrypoint_regexes:
                match = re.match(regex, entrypoint)
                if match is not None:
                    utype, uid = list(match.groupdict().items())[0]
                    logging.debug(f'regex={regex}\n'
                                  f'utype={utype}\n'
                                  f'uid={uid}')
                    if utype == 'image':
                        photos = [imgur_client.get_image(uid).link]
                    elif utype == 'album':
                        photos = [image['link'] for image in imgur_client.get_album(uid).images]
                    elif utype == 'gallery':
                        photos = [image['link'] for image in imgur_client.gallery_item(uid).images]
                    break
        logging.debug(f'photos={photos}')

        # send title and text_message
        bot.send_message(chat_id=telegram_channel,
                         text=f'<a href="{html.escape(reddit_link)}">{html.escape(title)}</a>\n\n'
                              f'{html.escape(text_message)}',
                         parse_mode=telegram.ParseMode.HTML,
                         disable_web_page_preview=True)
        logging.debug(textwrap.dedent(f'''
            bot.send_message(chat_id={telegram_channel},
                             text=f'<a href="{html.escape(reddit_link)}">{html.escape(title)}</a>\n\n'
                                  f'{html.escape(text_message)}',
                             parse_mode=telegram.ParseMode.HTML,
                             disable_web_page_preview=True)
        '''))

        # send photos
        for photo in photos:
            try:
                bot.send_photo(chat_id=telegram_channel, photo=photo)
            except telegram.error.BadRequest as e:
                logging.exception(e)
            finally:
                logging.debug(f'bot.send_photo(chat_id={telegram_channel}, photo={photo})')


if __name__ == '__main__':
    # new, hot, rising, top, controversial
    channels = [
        ('battlestations', -1001117849941, ['new']),
        ('jokes', -1001100859743, ['top']),
        ('wallpapers', -1001102302667, ['top']),
        ('wallpaper', -1001118977923, ['top']),
        ('battletops', -1001114529733, ['new']),
        ('minimalwallpaper', -1001087801782, ['top']),
        ('ultrahdwallpapers', -1001100944048, ['top']),
        ('verticalwallpapers', -1001078324620, ['top']),
        ('sexygirls', -1001112169795, ['top']),
        ('sexybutnotporn', -1001109782946, ['top']),
        ('minimalisticwalls', -1001086050860, ['top']),
        ('gamewalls', -1001085768542, ['top']),
    ]
    while True:
        for channel in channels:
            try:
                main(*channel)
            except Exception as e:
                logging.exception(e)
            time.sleep(60)

