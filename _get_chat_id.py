#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import requests
import telegram
import os
import json
import base64

if os.environ.get('HOST', '') != 'mark':
    secrets_url = 'http://vault-0.eightnoteight.smartzero:8200/v1/secret/alpha_artemis.eightnoteight.smartzero'
else:
    secrets_url = 'http://localhost:8200/v1/secret/alpha_artemis.eightnoteight.smartzero'


secrets = json.loads(str(base64.decodebytes(
    json.loads(requests.get(secrets_url,
                            headers={
                                # should be safe to expose this token as this container
                                # is only visible to other containers
                                'X-Vault-Token': '437ee1480c19482e66b97f8743e9be08'
                            }).content)['data']['data'].encode('ascii')), encoding='utf-8'))


bot =  bot = telegram.Bot(secrets['telegram_bot_api_token'])
message = bot.sendMessage('@' + sys.argv[1], '__')
print(message.chat_id)

